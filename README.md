# Vue 3 + Typescript + Vite2

# Installation dependency
`yarn install`

# Run
`yarn run dev`


# Build
`yarn run build`

#Adress
`localhost:12345`

# vue3 Document
https://v3.cn.vuejs.org/guide/installation.html

# TS Document
https://www.tslang.cn/docs/home.html

# vite Document
https://cn.vitejs.dev/config/#server-host

# prettier Document
https://prettier.io/docs/en/options.html

