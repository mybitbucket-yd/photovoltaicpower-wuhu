// @ts-nocheck
import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import routes from './routes'

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

router.beforeEach((to, from) => {
  let { path } = to
  sessionStorage.removeItem('GF_curouter')
  sessionStorage.setItem('GF_curouter', { path })
})

export default router
