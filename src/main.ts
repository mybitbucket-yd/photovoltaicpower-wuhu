import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import zhCn from 'element-plus/lib/locale/lang/zh-cn'
import './utils/flexible'
import * as Icons from '@element-plus/icons-vue'

const app = createApp(App)
// icon 图标
Object.keys(Icons).forEach(key => {
  app.component(key, Icons[key as keyof typeof Icons])
})
// 注册全局组件
const components: any = import.meta.globEager('./components/global/*.vue')
for (const key in components) {
  let k = key.match(/global\/(\S*)\.vue/)
  if (k && k.length > 1) {
    let name = 'G-' + k[1]
    // console.log(components[key])
    app.component(name, components[key].default)
  }
}

app.use(ElementPlus, { locale: zhCn }).use(router).use(store).mount('#app')
